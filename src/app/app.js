(function () {
	'using strict';

	angular.module('miniApp', ['ngRoute']);

	angular.module('miniApp').config([
		'$routeProvider',
		'$compileProvider',
		function ($routeProvider, $compileProvider) {
			$compileProvider.debugInfoEnabled(false);

			$routeProvider.
				when('/mini', {
					templateUrl: 'views/mini.html',
					controller: 'MiniController'
				}).
				otherwise({
					redirectTo: '/mini'
				});
		}]);
})();
