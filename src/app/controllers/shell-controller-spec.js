'use strict';

// Sample controller test
describe('ShellController', function () {
	beforeEach(module('miniApp'));

	var $controller;

	beforeEach(inject(function (_$controller_) {
		// The injector unwraps the underscores (_) from around the parameter
		// names when matching
		$controller = _$controller_;
	}));

	describe('$scope.model', function () {
		it('tests the $scope.shell.email default email address', function () {
			var $scope = {};
			var controller = $controller('ShellController', { $scope: $scope });

			expect($scope.shell.email).toEqual('miniApp@adp.com');
		});

		it('tests the $scope.model.hello name is blank', function () {
			var $scope = {};
			var controller = $controller('ShellController', { $scope: $scope });
      $scope.setEmail('test@test.com');

			expect($scope.shell.email).toEqual('test@test.com');
		});

	});
});
