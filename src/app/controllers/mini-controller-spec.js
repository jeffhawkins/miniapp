'use strict';

// Sample controller test
describe('MiniController', function () {
	beforeEach(module('miniApp'));

	var $controller;

	beforeEach(inject(function (_$controller_) {
		// The injector unwraps the underscores (_) from around the parameter
		// names when matching
		$controller = _$controller_;
	}));

	describe('$scope.model', function () {
		it('tests the $scope.model.hello default message', function () {
			var $scope = {};
			var controller = $controller('MiniController', { $scope: $scope });

			expect($scope.model.hello).toEqual('Hello from the MiniController.');
		});

		it('tests the $scope.model.hello name is blank', function () {
			var $scope = {};
			var controller = $controller('MiniController', { $scope: $scope });

			expect($scope.model.name).toEqual('');
		});

		it('tests the $scope.model.hello name is not blank', function () {
			var $scope = {};
			var controller = $controller('MiniController', { $scope: $scope });

			// Set the name on the scope.
			$scope.setName('Mini');

			expect($scope.model.name).toEqual('Mini');
		});
	});
});
