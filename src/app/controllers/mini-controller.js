(function () {
	'using strict';

	angular.module('miniApp').controller('MiniController', [
		'$scope',
		'$log',
		'$http',
		function ($scope, $log, $http) {
			$scope.model = {
				hello: 'Hello from the MiniController.',
				name: '' // Empty for now
			};

			$scope.setName = function(name) {
				$scope.model.name = name;
			};

			// Handle clickMe
			$scope.clickMe = function () {
				$log.info('You clicked me! ');

				var name = {
					name: $scope.model.name
				};

				$http.post('/rest/v1/name', name).then(function (response) {
					$log.info('data: ' + JSON.stringify(response));
					$scope.model.hello = 'Hello ' + response.data.name + ', how are you today?';
				});
			};
		}
	]);
})();
