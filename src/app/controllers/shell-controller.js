(function () {
	'using strict';

	angular.module('miniApp').controller('ShellController', [
		'$scope',
		'$log',
		'$http',
		function ($scope, $log, $http) {
			$scope.shell = {
				email: 'miniApp@adp.com'
			};

			$scope.setEmail = function(email) {
        $scope.shell.email = email;
      };

		}
	]);
})();
