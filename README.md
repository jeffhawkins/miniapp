## MiniApp

MiniApp is a simple AngularJS 1.5x application that can serve as a template for building mini apps with microservices.

#### Install

```
npm install grunt -g
npm install grunt-cli -g
npm install
grunt
```

Open a browser and navigate to http://localhost:3000

MiniApp has complete grunt support with the following commands:

```
grunt            # The default... builds and runs the application and watches for changes.

grunt production # Builds the app for production use. Minified and uglified.

grunt test       # Builds the application and runs the tests.
```


#### Mocks
The mini app has support for mocking using a *connect* server within Grunt. You can mock any response by creating a file with the correct naming convention in the mocks directory.

The naming convention is as simple as:

http method + uri (convert slashes to dashes) + .json, Ex:

- post-rest-v1-name.json
- get-rest-v1-name.json
- put-rest-v1-name.json