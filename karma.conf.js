'use strict';

module.exports = function(config) {
  config.set({
    basePath: '',
    plugins: ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-coverage'],
    frameworks: ['jasmine'],
    files: [
      './node_modules/angular/angular.min.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './node_modules/angular-route/angular-route.js',
      'src/**/*.js'
    ],
    exclude: [
      'karma.conf.js',
      'Gruntfile.js'
    ],
    coverageReporter: {
      type: 'text'
    },
    preprocessors: {
      'src/**/*-spec.js': ['coverage']
    },
    reporters: ['progress', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.DEBUG,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true
  });
};