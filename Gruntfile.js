
module.exports = function (grunt) {
  'use strict';

  var fs = require('fs');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: '\n'
      },
      js: {
        src: ['src/**/*.js', '!src/**/*-spec.js'],
        dest: 'static/<%= pkg.name %>.js'
      }
    },
    clean: ['./static'],
    less: {
      development: {
        files: {
          'static/css/main.css': 'src/less/main.less'
        }
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js', '!src/**/*-spec.js'],
      options: {
        esversion: 6,
        globals: {
          jQuery: true,
        }
      }
    },
    copy: {
      index: {
        expand: true,
        cwd: 'src',
        src: 'index.html',
        dest: 'static'
      },
      views: {
        expand: true,
        cwd: 'src/app/views',
        src: '*.html',
        dest: 'static/views'
      },
      bootstrap: {
        expand: true,
        cwd: 'node_modules',
        src: 'bootstrap/dist/**',
        dest: 'static/vendor'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd@H:MM") %> */\n'
      },
      production: {
        files: {
          'static/<%= pkg.name %>.min.js': ['static/<%= pkg.name %>.js']
        }
      }
    },
    replace: {
      development: {
        options: {
          patterns: [
            {
              match: 'timestamp',
              replacement: '<%= new Date().getTime() %>'
            },
            {
              match: 'jsfile',
              replacement: '<%= pkg.name %>.js'
            }
          ]
        },
        files: [
          { expand: true, flatten: true, src: ['static/index.html'], dest: 'static/' }
        ]
      },
      production: {
        options: {
          patterns: [
            {
              match: 'timestamp',
              replacement: '<%= new Date().getTime() %>'
            },
            {
              match: 'jsfile',
              replacement: '<%= pkg.name %>.min.js'
            }
          ]
        },
        files: [
          { expand: true, flatten: true, src: ['static/index.html'], dest: 'static/' }
        ]
      }
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        background: true,
        singleRun: false
      },
      continuous: {
        configFile: 'karma.conf.js'
      }
    },
    watch: {
      options: {
        livereload: true
      },
      files: ['Gruntfile.js', 'src/**/*.js', 'src/**/*.less', 'src/**/*.html'],
      tasks: ['clean', 'jshint', 'karma', 'less', 'copy', 'concat', 'replace:development'],
      cwd: '/static'
    },
    connect: {
      server: {
        options: {
          port: 3000,
          base: 'static',
          middleware: function (connect, options, middlewares) {
            middlewares.unshift(function (req, res, next) {
              var mock = req.url.replace(/\//g, '-');
              var filename = './mocks/' + req.method.toLowerCase() + mock + '.json';
              fs.readFile(filename, 'utf8', function (err, data) {
                if (err) {
                  console.log('No mock found for request - ' + req.method + ' ' + req.url);
                  return next();
                } else {
                  console.log('Mock found for request - ' + req.method + ' ' + req.url + ' (' + filename + ')');
                }
                
                res.setHeader('Content-Type', 'application/json');
                res.end(data);
              });
            });

            return middlewares;
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-replace');

  grunt.registerTask('default', [
    'clean',
    'jshint',
    'karma',
    'less',
    'copy',
    'concat',
    'replace:development',
    'connect',
    'watch'
  ]);

  grunt.registerTask('production', [
    'clean',
    'jshint',
    'karma:continuous',
    'less',
    'copy',
    'concat',
    'uglify:production',
    'replace:production'
  ]);

  grunt.registerTask('test', [
    'jshint',
    'karma'
  ]);
};